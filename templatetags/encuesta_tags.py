from django import template
from django.template.loader import render_to_string
from django.utils.html import format_html
from apps.Encuesta.models import *
from apps.Encuesta import views
register = template.Library()

@register.simple_tag(takes_context=True)
def encuesta(context):
	request = context['request']

	try:
		encuesta = Encuesta.publicada.latest("fecha")
	except Encuesta.DoesNotExists:
		return ''

	items = Item.objects.filter(encuesta=encuesta)

	if encuesta.get_cookie_name() in request.COOKIES:
		template = "encuestas/result.html"
	else:
		template = "encuestas/poll.html"

	content = render_to_string(template, {
		'encuesta': encuesta,
		'items': items,
	})

	return content

@register.simple_tag
def percentage(pregunta, item):
	pregunta_vote_count = pregunta.get_vote_count()
	if pregunta_vote_count > 0:
		num=float(item.votos) / float(pregunta_vote_count) * 100
		return round(num,2)
	else:
		return 0