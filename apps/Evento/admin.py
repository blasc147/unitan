from django.contrib import admin
from django.core.mail import send_mail
from .models import Evento, Email

class EmailInline(admin.TabularInline):
    model = Email
    extra = 0

class EventoAdmin(admin.ModelAdmin):
    list_display = ('titulo','fecha', 'area', 'sede')
    list_filter = ('area', 'fecha')
    icon = '<i class="material-icons">today</i>'
    fields = ('titulo', 'descripcion', 'fecha','imagen', 'area', 'sede')
    inlines = [ EmailInline, ]
    def save_model(self, request, obj, form, change):      
        lista=Email.objects.filter(evento_id = obj.pk)
        for e in lista:
            send_mail(
                    'Aviso de nuevo evento : '+obj.titulo,
                    str(obj.fecha)+' - '+obj.descripcion,
                    'from@example.com',
                    [e.email],
                    fail_silently=False,
                    )
            print(e.email)
        if not change: 
        # can use this condition also to set 'created_by'    
        # if not getattr(obj, 'created_by', None):            
            obj.creado_por = request.user
        obj.save()

admin.site.register(Evento,EventoAdmin)