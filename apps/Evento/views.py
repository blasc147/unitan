from django.shortcuts import render
from django.views.generic.edit import CreateView, DeleteView, UpdateView
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.contrib.auth.mixins import LoginRequiredMixin,PermissionRequiredMixin
from django.core.urlresolvers import reverse_lazy
from django.contrib.auth.mixins import PermissionRequiredMixin
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse, Http404
from apps.usuarios.models import Usuario
from apps.core.decorators import group_required
from .models import Evento
from apps.Area.models import Area
from apps.Sede.models import Sede
from django.core import serializers

import json

class ListarEventos(ListView):
	model = Evento
	template_name = 'eventos/listaEventos.html'
	context_object_name = "Eventos"
	paginate_by = 10
	def get_queryset(self):
		n = Evento.objects.order_by('-fecha')
		return n
	def get_context_data(self,**kwargs):
		context = super(ListarEventos,self).get_context_data(**kwargs)
		context['areas'] = Area.objects.all()
		context['sedes'] = Sede.objects.all()
		paginator=context['paginator']
		pagActual=context['page_obj'].number
		cantPag=len(paginator.page_range)
		start=pagActual-2 if pagActual >= 2 else 0
		final= pagActual+2 if pagActual <= cantPag-2 else cantPag
		context['paginacion']=list(paginator.page_range)[start:final]
		return context

class Detalle_Evento(DetailView):
	model = Evento
	template_name = "eventos/detalleEvento.html"
	def get_context_data(self,**kwargs):
		context = super(Detalle_Evento,self).get_context_data(**kwargs)
		context['areas'] = Area.objects.all()
		context['sedes'] = Sede.objects.all()
		return context