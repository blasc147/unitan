from django.db import models
from apps.core.models import TimeStampedModel
from apps.usuarios.models import Usuario
from apps.Area.models import Area
from apps.Sede.models import Sede

class Evento(TimeStampedModel):
	titulo = models.CharField(max_length=60,
								verbose_name=u'Titulo',
								help_text=u'Titulo de Evento')
	descripcion = models.CharField(max_length=300,
									verbose_name=u'Resumen',
									help_text=u'Resumen de Evento')
	fecha_creacion = models.DateField(verbose_name=u'Fecha',auto_now_add=True)

	fecha = models.DateField(verbose_name=u'Fecha')

	creado_por = models.ForeignKey(Usuario)

	imagen = models.ImageField(upload_to = "evento_imagen",  null = True, blank=True)

	area= models.ForeignKey(Area , null = True, blank=True)
	sede= models.ForeignKey(Sede , null = True, blank=True)

	class Meta:
		verbose_name = 'Evento'
		verbose_name_plural = 'Eventos'

class Comentario(models.Model):
	mensaje = models.TextField(max_length=200,
                              verbose_name='comentario',
                              help_text='Comentario')
	novedad = models.ForeignKey(Evento)

	def __str__(self):
	    return self.nombre

	class Meta:
	    verbose_name = 'comentario'
	    verbose_name_plural = 'comentarios'

class Email(models.Model):
	nombre = models.CharField(max_length=30,
								verbose_name=u'Nombre')
	evento = models.ForeignKey(Evento, related_name='evento_mail')
	email = models.EmailField(max_length=150, null = False, blank=False)

	class Meta:
		ordering = ["pk"]