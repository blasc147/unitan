# -*- coding: utf-8 -*-
# Generated by Django 1.10 on 2017-12-05 15:47
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('Sede', '0001_initial'),
        ('Evento', '0004_comentario'),
    ]

    operations = [
        migrations.AddField(
            model_name='evento',
            name='sede',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='Sede.Sede'),
        ),
    ]
