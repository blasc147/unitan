from django.conf.urls import url, include
from . import views


urlpatterns = [
    url(r'^eventos/', views.ListarEventos.as_view(), name='listarEventos'),
    url(r'^detalleEvento/(?P<pk>[0-9]+)/', views.Detalle_Evento.as_view(), name='detalle_evento'),

]
