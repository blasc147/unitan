from django.contrib.auth.forms import UserCreationForm
from .models import Usuario
from django import forms

class CreateUser(UserCreationForm):
	def __init__(self, *args, **kwargs):
		super(CreateUser, self).__init__(*args, **kwargs)
		self.fields['username'].widget.attrs.pop("autofocus", None)
		self.fields['avatar'].widget.attrs.update({'class':'btn'})
	class Meta:
		model = Usuario
		fields = ['username','first_name','last_name','email','avatar']