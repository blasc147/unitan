from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from .models import Usuario
# Register your models here.

class UserAdmin(UserAdmin):
    icon = '<i class="material-icons">perm_identity</i>'

admin.site.register(Usuario,UserAdmin)