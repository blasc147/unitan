from django import forms
from .models import Novedad, Comentario

class formComentario(forms.ModelForm):

	nombre = forms.CharField(required = False, widget=forms.TextInput(attrs={
					'class':'form-control',
                            	}))
	mensaje = forms.CharField(widget=forms.TextInput(attrs={
					'class':'textarea-message form-control',
		}))
	
	class Meta:
		model = Comentario
		exclude = ['novedad', 'fecha']