from django.conf.urls import url, include
from . import views


urlpatterns = [
    url(r'^novedades/', views.ListarNovedades.as_view(), name='listarNovedades'),
    url(r'^novedadesAnteriores/', views.ListarNovedadesViejas.as_view(), name='listarNovedadesViejas'),
    url(r'^detalleNoticia/(?P<pk>[0-9]+)/', views.Detalle_Novedad.as_view(), name='detalle_novedad'),
    url(r'^like-blog/$', views.like_count_blog, name='like_count_blog'),
    url(r'^Comentarios/Nuevo/(?P<pk>[0-9]+)/', views.Crear_Comentario, name='crear_comentario'),

]
