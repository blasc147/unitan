from django.shortcuts import render
from django.views.generic.edit import CreateView, DeleteView, UpdateView
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.contrib.auth.mixins import LoginRequiredMixin,PermissionRequiredMixin
from django.core.urlresolvers import reverse_lazy
from django.contrib.auth.mixins import PermissionRequiredMixin
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse, Http404
from apps.usuarios.models import Usuario
from apps.core.decorators import group_required
from .models import Novedad, Comentario, Foto
from .forms import formComentario
from apps.Area.models import Area
from apps.Sede.models import Sede
from django.core import serializers
from datetime import datetime, timedelta

import json

class ListarNovedades(ListView):
	model = Novedad	
	template_name = 'novedades/listaNovedades.html'
	paginate_by = 18
	context_object_name = "Novedad"
	def get_queryset(self):
		last_month = datetime.today() - timedelta(days=90)
		n = Novedad.objects.filter(fecha_creacion__gte=last_month).order_by('-fecha_creacion')
		return n
	def get_context_data(self,**kwargs):
		context = super(ListarNovedades,self).get_context_data(**kwargs)
		last_month = datetime.today() - timedelta(days=90)
		context['areas'] = Area.objects.all()
		context['sedes'] = Sede.objects.all()
		context['anteriores'] = Novedad.objects.exclude(fecha_creacion__gte=last_month).order_by('-fecha_creacion')[:10]
		paginator=context['paginator']
		pagActual=context['page_obj'].number
		cantPag=len(paginator.page_range)
		start=pagActual-2 if pagActual >= 2 else 0
		final= pagActual+2 if pagActual <= cantPag-2 else cantPag
		context['paginacion']=list(paginator.page_range)[start:final]
		return context

class ListarNovedadesViejas(ListView):
	model = Novedad	
	template_name = 'novedades/listaNovedadesViejas.html'
	paginate_by = 18
	context_object_name = "Novedad"
	def get_queryset(self):
		last_month = datetime.today() - timedelta(days=90)
		n = Novedad.objects.exclude(fecha_creacion__gte=last_month).order_by('-fecha_creacion')
		return n
	def get_context_data(self,**kwargs):
		context = super(ListarNovedadesViejas,self).get_context_data(**kwargs)
		context['areas'] = Area.objects.all()
		context['sedes'] = Sede.objects.all()
		paginator=context['paginator']
		pagActual=context['page_obj'].number
		cantPag=len(paginator.page_range)
		start=pagActual-2 if pagActual >= 2 else 0
		final= pagActual+2 if pagActual <= cantPag-2 else cantPag
		context['paginacion']=list(paginator.page_range)[start:final]
		return context

class Detalle_Novedad(DetailView):
	model = Novedad
	template_name = "novedades/detalleNoticia.html"
	def get_context_data(self,**kwargs):
		context = super(Detalle_Novedad,self).get_context_data(**kwargs)
		novedad = Novedad.objects.get(id=self.kwargs['pk'])
		novedad.visitas+=1
		novedad.save()
		context['areas'] = Area.objects.all()
		context['comentarios']= Comentario.objects.filter(novedad_id = self.kwargs['pk'])
		context['imagenes'] = Foto.objects.filter(novedad_id = self.kwargs['pk'])
		return context

def like_count_blog(request):
	liked = False
	if request.is_ajax():
		novedad_id = request.GET.get('novedad_id', None)
		novedad = Novedad.objects.get(pk=novedad_id)
		request.session['has_liked_'+novedad_id] = True
		likes = novedad.likes + 1
		novedad.likes = likes
		novedad.save()
		data = json.dumps(novedad.likes)
		mimetype = 'application/json'
		return HttpResponse(data, mimetype)
	else:
		raise Http404

def Crear_Comentario(request,pk):
	if request.is_ajax() and request.method == 'POST':
		n = Novedad.objects.get(pk = pk)
		f = formComentario(request.POST)
		if f.is_valid():
			comentario = f.save(commit = False)
			comentario.novedad = n
			try:
				comentario.save()
				error = None
			except Exception as a:
			 	error = a
		if error:
			data = json.dumps('error')
		else:
			nueva = {}
			nueva['id'] = comentario.pk
			nueva['nombre'] = comentario.nombre
			nueva['mensaje'] = comentario.mensaje
			nueva['clave'] = comentario.pk
		data = json.dumps(nueva)
		mimetype = 'application/json'
		return HttpResponse(data, mimetype)
	else:
		raise Http404
