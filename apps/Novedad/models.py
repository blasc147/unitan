from django.db import models
from apps.core.models import TimeStampedModel
from apps.usuarios.models import Usuario
from apps.Area.models import Area
from apps.Sede.models import Sede
from embed_video.fields import EmbedVideoField

class Novedad(TimeStampedModel):
	titulo = models.CharField(max_length=60,
								verbose_name=u'Titulo',
								help_text=u'Titulo de Noticia')

	descripcion = models.TextField(max_length=1000,
									verbose_name=u'Descripcion',
									help_text=u'Descripcion de Noticia')
	fecha_creacion = models.DateField(verbose_name=u'Fecha')

	creado_por = models.ForeignKey(Usuario)

	imagen_portada = models.ImageField(upload_to = "noticia_portada",  null = True, blank=True)

	pdf = models.FileField(upload_to='pdfs',null = True, blank=True)

	area= models.ForeignKey(Area, null = True, blank=True)

	sede= models.ForeignKey(Sede , null = True, blank=True)

	likes = models.PositiveIntegerField(default=0, verbose_name=u'Cant. likes')

	visitas = models.PositiveIntegerField(default=0, verbose_name=u'Visitas')

	url= models.URLField(max_length=200, null = True, blank=True, help_text=u'URL del Drive')

	video = models.FileField(upload_to='videos',null = True, blank=True, help_text=u'Archivo de video')

	def __str__(self):
		return self.titulo
	@property

	def total_likes(self):
		return self.likes.count()

	class Meta:
		verbose_name = 'Novedad'
		verbose_name_plural = 'Novedades'

class Comentario(models.Model):
	nombre = models.CharField(max_length=60,
								verbose_name=u'Nombre', default = "Anonimo")
	mensaje = models.TextField(max_length=200,verbose_name='comentario',help_text='Comentario')

	novedad = models.ForeignKey(Novedad)

	fecha = models.DateField(verbose_name=u'Fecha',auto_now_add=True, blank=True,)
	def __str__(self):
		return self.nombre

	class Meta:
		verbose_name = 'comentario'
		verbose_name_plural = 'comentarios'

class Foto(models.Model):
	nombre = nombre = models.CharField(max_length=30,
								verbose_name=u'Nombre')
	novedad = models.ForeignKey(Novedad, related_name='novedad_foto')
	imagen = models.ImageField(null = False, blank=False)

	class Meta:
		ordering = ["pk"]