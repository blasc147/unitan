from django.contrib import admin
from embed_video.admin import AdminVideoMixin
from .models import Novedad, Foto, Comentario

class FotoInline(admin.TabularInline):
    model = Foto
    extra = 0
    
class NovedadAdmin(AdminVideoMixin, admin.ModelAdmin):
    list_display = ('titulo', 'sede','area', 'likes', 'visitas')
    list_filter = ('area', 'fecha_creacion', 'sede')
    icon = '<i class="material-icons">art_track</i>'
    fields = ('titulo', 'descripcion','fecha_creacion','imagen_portada', 'area', 'sede', 'pdf', 'url', 'video')
    inlines = [ FotoInline, ]
    #filter_horizontal = ('developers',)
    def save_model(self, request, obj, form, change):             
        if not change: 
        # can use this condition also to set 'created_by'    
        # if not getattr(obj, 'created_by', None):            
            obj.creado_por = request.user
        obj.save()


    
class ComentarioAdmin(admin.ModelAdmin):
    icon = '<i class="material-icons">place</i>'
    list_filter = ('novedad', )
    model= Comentario
    #filter_horizontal = ('developers',)

admin.site.register(Comentario,ComentarioAdmin)
admin.site.register(Novedad,NovedadAdmin)