from django.shortcuts import render
from django.views.generic.edit import CreateView, DeleteView, UpdateView
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.contrib.auth.mixins import LoginRequiredMixin,PermissionRequiredMixin
from django.core.urlresolvers import reverse_lazy
from django.contrib.auth.mixins import PermissionRequiredMixin
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse, Http404
from apps.usuarios.models import Usuario
from apps.core.decorators import group_required
from .models import Sede
from apps.Novedad.models import Novedad
from apps.Area.models import Area
from apps.Evento.models import Evento
from apps.Novedad.models import Novedad
from django.core import serializers

import json
# Create your views here.
class EventosSede(DetailView):
	model = Sede
	template_name = "sedes/eventosSede.html"
	def get_context_data(self,**kwargs):
		context = super(EventosSede,self).get_context_data(**kwargs)
		context['eventos'] = Evento.objects.filter(sede_id = self.kwargs['pk']).order_by('-fecha_creacion')
		context['areas'] = Area.objects.all()
		context['sedes'] = Sede.objects.all()
		return context

class NovedadesSede(DetailView):
	model = Sede
	template_name = "sedes/novedadesSede.html"
	def get_context_data(self,**kwargs):
		context = super(NovedadesSede,self).get_context_data(**kwargs)
		context['novedades'] = Novedad.objects.filter(sede_id = self.kwargs['pk']).order_by('-fecha_creacion')
		context['areas'] = Area.objects.all()
		context['sedes'] = Sede.objects.all()
		return context