from django.contrib import admin

from .models import Sede
from . import models

class SedeAdmin(admin.ModelAdmin):
    icon = '<i class="material-icons">place</i>'
    model= Sede
    #filter_horizontal = ('developers',)

admin.site.register(Sede,SedeAdmin)