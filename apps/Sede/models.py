from django.db import models

# Create your models here.
class Sede(models.Model):
    nombre = models.CharField(max_length=60,
                              verbose_name='nombre',
                              help_text='Nombre de la sede')

    def __str__(self):
        return self.nombre

    class Meta:
        verbose_name = 'sede'
        verbose_name_plural = 'sedes'