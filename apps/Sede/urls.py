from django.conf.urls import url, include
from . import views


urlpatterns = [
	url(r'^eventossede/(?P<pk>[0-9]+)/', views.EventosSede.as_view(), name='eventosSede'),
	url(r'^novedadessede/(?P<pk>[0-9]+)/', views.NovedadesSede.as_view(), name='novedadesSede'),

]
