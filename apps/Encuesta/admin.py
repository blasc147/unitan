from django.contrib import admin
from .models import Encuesta, Item, Vote, Pregunta
# Register your models here.
class ItemInline(admin.TabularInline):
    model = Item
    extra = 1
    max_num = 15
    exclude = ('votos',)
    verbose_name = u'item'
    verbose_name_plural = u'item'

class PreguntaInLine(admin.TabularInline):
    
    model = Pregunta
    extra =1
    max_num = 15
    verbose_name = u'pregunta'
    verbose_name_plural = u'preguntas'

class PreguntaAdmin(admin.ModelAdmin):
    
    list_display = ('pregunta', 'encuesta')
    list_filter = ('encuesta',)
    inlines = [ItemInline,]
    fields = ('encuesta','pregunta')

#class ItemAdmin(admin.ModelAdmin):
    
#    list_display = ('value', 'votos','pregunta')
#    fields = ('value','pos')


    
class EncuestaAdmin(admin.ModelAdmin):
    icon = '<i class="material-icons">list</i>'
    list_display = ('titulo', 'fecha', 'vote_count', 'publicada')
    fields = ('titulo','fecha', 'area','publicada')
    inlines = [PreguntaInLine, ]

    def save_model(self, request, obj, form, change):             
        if not change: 
        # can use this condition also to set 'created_by'    
        # if not getattr(obj, 'created_by', None):            
            obj.creado_por = request.user
        obj.save()



admin.site.register(Encuesta, EncuestaAdmin)
admin.site.register(Pregunta, PreguntaAdmin)
#admin.site.register(Item, ItemAdmin)


