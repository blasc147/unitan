from django.db import models
from apps.core.models import TimeStampedModel
from apps.usuarios.models import Usuario
from apps.Area.models import Area
from django.db.models import Sum

class Encuesta(TimeStampedModel):
	titulo = models.CharField(max_length=60,
								verbose_name=u'Titulo',
								help_text=u'Titulo de Encuesta')

	creado_por = models.ForeignKey(Usuario)

	fecha = models.DateField(verbose_name=u'Fecha')

	area= models.ForeignKey(Area , null = True, blank=True)

	publicada = models.BooleanField(default=True, verbose_name=u'publicada')

	class Meta:
		ordering = ['-fecha']
		verbose_name = 'Encuesta'
		verbose_name_plural = 'Encuestas'

	def __str__(self):
		return self.titulo

	def get_vote_count(self):
		cant_preguntas = Pregunta.objects.filter(encuesta=self).count()
		preguntas = Pregunta.objects.filter(encuesta=self)
		contador = 0
		for p in preguntas:
			if p.get_vote_count() is not None:
				contador = contador + p.get_vote_count()
		if cant_preguntas > 0:
			return int(contador/cant_preguntas)
		else:
			return 0
	vote_count = property(fget=get_vote_count)

class Pregunta(models.Model):
	encuesta = models.ForeignKey(Encuesta)
	pregunta = models.CharField(max_length=300,
									verbose_name=u'Pregunta',)

	
	class Meta:
		verbose_name = 'Pregunta'
		verbose_name_plural = 'Preguntas'

	def __str__(self):
		return self.pregunta

	def get_vote_count(self):
		itemSum = Item.objects.filter(pregunta=self).aggregate(Sum('votos'))
		return itemSum['votos__sum']
	vote_count = property(fget=get_vote_count)

class Item(models.Model):
	pregunta = models.ForeignKey(Pregunta, on_delete=models.CASCADE, null=True)
	value = models.CharField(max_length=250, verbose_name=u'opcion')
	pos = models.SmallIntegerField(default='0', verbose_name=u'position')
	votos = models.IntegerField(default='0', verbose_name=u'votos')

	class Meta:
		verbose_name ='respuesta'
		verbose_name_plural = 'respuestas'
		ordering = ['pos']

	def __str__(self):
		return u'%s' % (self.value,)
	
	def save(self, *args, **kwargs):
		self.encuesta = self.pregunta.encuesta
		super().save(*args, **kwargs)

	def get_vote_count(self):
		return Vote.objects.filter(item=self).count()
	vote_count = property(fget=get_vote_count)

class Vote(models.Model):
	encuesta = models.ForeignKey(Encuesta, verbose_name=u'encuesta')
	item = models.ForeignKey(Item, verbose_name=u'voted item')

	class Meta:
		verbose_name = 'voto'
		verbose_name_plural = 'votos'
