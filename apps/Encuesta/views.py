from django.shortcuts import render, redirect
from django.views.generic.edit import CreateView, DeleteView, UpdateView
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.contrib.auth.mixins import LoginRequiredMixin,PermissionRequiredMixin
from django.core.urlresolvers import reverse_lazy
from django.contrib.auth.mixins import PermissionRequiredMixin
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse, Http404
from apps.usuarios.models import Usuario
from apps.core.decorators import group_required
from .models import Encuesta, Item, Vote
from apps.Area.models import Area
from django.core import serializers
from django.http import HttpResponse, Http404


class ListarEncuestas(ListView):
	model = Encuesta	
	template_name = 'encuestas/listaEncuestas.html'
	paginate_by = 10
	context_object_name = "Encuesta"
	def get_queryset(self):
		n = Encuesta.objects.all().filter(publicada=True)
		return n
	def get_context_data(self,**kwargs):
		context = super(ListarEncuestas,self).get_context_data(**kwargs)
		context['areas'] = Area.objects.all()
		paginator=context['paginator']
		pagActual=context['page_obj'].number
		cantPag=len(paginator.page_range)
		start=pagActual-2 if pagActual >= 2 else 0
		final= pagActual+2 if pagActual <= cantPag-2 else cantPag
		context['paginacion']=list(paginator.page_range)[start:final]
		return context

# Create your views here.
class Detalle_Encuesta(DetailView):
	model = Encuesta
	template_name = "encuestas/detalleEncuestas.html"
	def get_context_data(self,**kwargs):
		context = super(Detalle_Encuesta,self).get_context_data(**kwargs)
		context['areas'] = Area.objects.all()
		return context

class Resultado_Encuesta(DetailView):
	model = Encuesta
	template_name = "encuestas/resultadoEncuesta.html"
	def get_context_data(self,**kwargs):
		context = super(Resultado_Encuesta,self).get_context_data(**kwargs)
		context['areas'] = Area.objects.all()
		return context


#def vote(request, pk):
#	if request.is_ajax():
#		try:
#			encuesta = Encuesta.objects.get(pk=pk)
#		except:
#			return HttpResponse('Wrong parameters', status=400)
#
#		item_pk = request.GET.get("item", False)
#		if not item_pk:
#			return HttpResponse('Wrong parameters', status=400)
#
#		try:
#			item = Item.objects.get(pk=item_pk)
#		except:
#			return HttpResponse('Wrong parameters', status=400)
#
#		Vote.objects.create(
#			encuesta=encuesta,
#			item=item,
#		)
#
#		response = HttpResponse(status=200)
#		#set_cookie(response, encuesta.get_cookie_name(), encuesta_pk)
#
#		return response
#	return HttpResponse(status=400)

def vote(request, pk):
	encuesta = Encuesta.objects.get(pk=pk)
	if request.method == 'POST':
		item_pk = request.POST.get("item", False)

		if not item_pk:
			return HttpResponse('Wrong parameters', status=400)

		try:
			item = Item.objects.get(pk=item_pk)
		except:
			return HttpResponse('Wrong parameters', status=400)
		
		Vote.objects.create(
			encuesta=encuesta,
			item=item,
		)

		return redirect(reverse_lazy('Encuesta:encuesta_result', args=(encuesta.pk,)))

from django.http import JsonResponse

def opcion(request):
	if request.is_ajax():
		
		item_pk = request.GET.get('id_item',None)
		
		if not item_pk:
			return HttpResponse('Wrong parameters', status=400)

		try:
			item = Item.objects.get(pk=item_pk)
		except:
			return HttpResponse('Wrong parameters', status=400)
		
		item.votos=item.votos+1
		item.save()

		response = HttpResponse(status=200)

		return response
	else:
		raise Http404
