from django.conf.urls import url, include
from . import views


urlpatterns = [
    url(r'^encuestas/', views.ListarEncuestas.as_view(), name='listarEncuestas'),
    url(r'^poll/(?P<pk>[0-9]+)/', views.Detalle_Encuesta.as_view(), name='detalle_encuesta'),
	url(r'^result/(?P<pk>[0-9]+)/', views.Resultado_Encuesta.as_view(), name='encuesta_result'),
	url(r'^vote/(?P<pk>[0-9]+)/', views.vote, name='ajax_vote'),
	url(r'^opcion/$', views.opcion, name='seleccionOpcion'),


]