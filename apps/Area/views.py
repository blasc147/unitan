from django.shortcuts import render
from django.views.generic.edit import CreateView, DeleteView, UpdateView
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.contrib.auth.mixins import LoginRequiredMixin,PermissionRequiredMixin
from django.core.urlresolvers import reverse_lazy
from django.contrib.auth.mixins import PermissionRequiredMixin
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse, Http404
from apps.usuarios.models import Usuario
from apps.core.decorators import group_required
from .models import Area
from apps.Novedad.models import Novedad
from apps.Evento.models import Evento
from apps.Sede.models import Sede
from apps.Encuesta.models import Encuesta
from django.core import serializers
from datetime import datetime, timedelta

import json


class NovedadesArea(ListView):
	model = Area
	paginate_by = 10
	template_name = "areas/novedadesArea.html"
	def get_context_data(self,**kwargs):
		context = super(NovedadesArea,self).get_context_data(**kwargs)
		context['area'] = Area.objects.get(id=self.kwargs['pk'])
		last_month = datetime.today() - timedelta(days=60)
		context['novedades'] = Novedad.objects.filter(area_id = self.kwargs['pk']).order_by('-fecha_creacion')
		context['sedes'] = Sede.objects.all()
		context['anteriores'] = Novedad.objects.filter(area_id = self.kwargs['pk']).exclude(fecha_creacion__gte=last_month).order_by('-fecha_creacion')[:10]
		paginator=context['paginator']
		pagActual=context['page_obj'].number
		cantPag=len(paginator.page_range)
		start=pagActual-2 if pagActual >= 2 else 0
		final= pagActual+2 if pagActual <= cantPag-2 else cantPag
		context['paginacion']=list(paginator.page_range)[start:final]
		context['areas'] = Area.objects.all()
		return context

class EventosArea(ListView):
	model = Area
	paginate_by = 10
	template_name = "areas/eventosArea.html"
	def get_context_data(self,**kwargs):
		context = super(EventosArea,self).get_context_data(**kwargs)
		context['area'] = Area.objects.get(id=self.kwargs['pk'])
		context['eventos'] = Evento.objects.filter(area_id = self.kwargs['pk']).order_by('-fecha_creacion')
		context['areas'] = Area.objects.all()
		context['sedes'] = Sede.objects.all()
		paginator=context['paginator']
		pagActual=context['page_obj'].number
		cantPag=len(paginator.page_range)
		start=pagActual-2 if pagActual >= 2 else 0
		final= pagActual+2 if pagActual <= cantPag-2 else cantPag
		context['paginacion']=list(paginator.page_range)[start:final]
		return context

class EncuestaArea(ListView):
	model = Area	
	template_name = 'areas/encuestasArea.html'
	paginate_by = 10
	def get_context_data(self,**kwargs):
		context = super(EncuestaArea,self).get_context_data(**kwargs)
		context['area'] = Area.objects.get(id=self.kwargs['pk'])
		context['encuestas'] = Encuesta.objects.filter(area_id = self.kwargs['pk'])
		context['areas'] = Area.objects.all()
		paginator=context['paginator']
		pagActual=context['page_obj'].number
		cantPag=len(paginator.page_range)
		start=pagActual-2 if pagActual >= 2 else 0
		final= pagActual+2 if pagActual <= cantPag-2 else cantPag
		context['paginacion']=list(paginator.page_range)[start:final]
		return context