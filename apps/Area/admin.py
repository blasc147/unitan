from django.contrib import admin

from .models import Area
from . import models

class AreaAdmin(admin.ModelAdmin):
    icon = '<i class="material-icons">device_hub</i>'
    model= Area
    #filter_horizontal = ('developers',)

admin.site.register(Area,AreaAdmin)