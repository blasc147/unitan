from django.conf.urls import url, include
from . import views


urlpatterns = [
	url(r'^novedadesarea/(?P<pk>[0-9]+)/', views.NovedadesArea.as_view(), name='novedadesArea'),
	url(r'^eventosarea/(?P<pk>[0-9]+)/', views.EventosArea.as_view(), name='eventosArea'),
	url(r'^encuesta.area/(?P<pk>[0-9]+)/', views.EncuestaArea.as_view(), name='encuestaArea'),


]
