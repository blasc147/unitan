from django.db import models

class Documento(models.Model):
	titulo = models.CharField(max_length=60,
								verbose_name=u'Titulo',
								help_text=u'Titulo de Noticia')

	descripcion = models.TextField(max_length=1000,
									verbose_name=u'Descripcion',
									help_text=u'Descripcion de Noticia')

	fecha_creacion = models.DateField(verbose_name=u'Fecha')

	pdf = models.FileField(upload_to='pdfs',null = True, blank=True)

	url= models.URLField(max_length=200, null = True, blank=True, help_text=u'URL del Drive')

	def __str__(self):
		return self.titulo

	class Meta:
		verbose_name = 'Documento'
		verbose_name_plural = 'Documentos'
