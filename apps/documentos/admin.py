from django.contrib import admin

from .models import Documento

class DocumentoAdmin(admin.ModelAdmin):
    list_display = ('titulo','fecha_creacion')
    icon = '<i class="material-icons">insert_drive_file</i>'
    fields = ('titulo', 'descripcion', 'fecha_creacion','pdf','url')

admin.site.register(Documento,DocumentoAdmin)