from django.shortcuts import render
from .models import Documento
from django.views.generic.list import ListView


class ListarDocumentos(ListView):
	model = Documento	
	template_name = 'documentos/listaDocumentos.html'
	paginate_by = 10
	context_object_name = "Documento"
	def get_queryset(self):
		n = Documento.objects.order_by('-fecha_creacion')
		return n
	def get_context_data(self,**kwargs):
		context = super(ListarDocumentos,self).get_context_data(**kwargs)
		#context['areas'] = Area.objects.all()
		#context['sedes'] = Sede.objects.all()
		paginator=context['paginator']
		pagActual=context['page_obj'].number
		cantPag=len(paginator.page_range)
		start=pagActual-2 if pagActual >= 2 else 0
		final= pagActual+2 if pagActual <= cantPag-2 else cantPag
		context['paginacion']=list(paginator.page_range)[start:final]
		return context