import os
import sys

from os.path import join, abspath, dirname
from sys import path
from dj_static import Cling

#sys.setdefaultencoding('utf-8')

os.environ['LANG']='en_ES.UTF-8'
os.environ['LC_ALL']='en_ES.UTF-8'

#SITE_ROOT = abspath(join(dirname(dirname(abspath(__file__))), 'QuAgi'))
#SITE_ROOT = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
#print(SITE_ROOT)
#path.append(SITE_ROOT)

from django.core.wsgi import get_wsgi_application
os.environ["DJANGO_SETTINGS_MODULE"] = "unitan.settings.production" 

application = Cling(get_wsgi_application())

