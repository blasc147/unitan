from .base import *

DEBUG=True

ALLOWED_HOSTS = ['*']
DATABASES = {
    'default': {        
        'ENGINE': 'django.db.backends.postgresql_psycopg2',       
        'NAME': 'unitan',
        'USER': 'unitanusr',
        'PASSWORD': 'unitan-pass',
        'HOST': 'localhost',
        'PORT': '5432',
        'DEFAULT_CHARSET': 'utf-8',
    }
}