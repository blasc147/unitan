from .base import *

DEBUG=True

ALLOWED_HOSTS = ['*']
DATABASES = {
    'default': {        
        'ENGINE': 'django.db.backends.postgresql_psycopg2',       
        'NAME': 'Unitan',
        'USER': 'unitan-usr',
        'PASSWORD': 'unitan-pass',
        'HOST': 'localhost',
        'PORT': '',
        'DEFAULT_CHARSET': 'utf-8',
    }
}
