"""unitan URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from django.conf.urls.static import static
from django.conf import settings
from .views import Start, institucional
from apps.Novedad import views

from django.contrib.auth import views as auth_views

urlpatterns = [
    url(r'^admin/', admin.site.urls),

    url(r'^institucional/', institucional, name='institucional'),

    url(r'^$', views.ListarNovedades.as_view(), name='listarNovedades'),

    url(r'^Novedades/', view=include('apps.Novedad.urls', namespace='Novedad')),

    url(r'^Documentos/', view=include('apps.documentos.urls', namespace='Documento')),

    url(r'^Eventos/', view=include('apps.Evento.urls', namespace='Evento')),

    url(r'^Areas/', view=include('apps.Area.urls', namespace='Area')),

    url(r'^Sedes/', view=include('apps.Sede.urls', namespace='Sede')),

    url(r'^Encuestas/', view=include('apps.Encuesta.urls', namespace='Encuesta')),
    
    #url(r'hitcount/', include('hitcount.urls', namespace='hitcount')),

]+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)